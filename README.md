[![Build Status](https://github.com/ltheinrich/etopa/workflows/CI/badge.svg)](https://github.com/ltheinrich/etopa/actions?query=workflow%3ACI)
[![Matrix](https://img.shields.io/matrix/etopa:matrix.org?label=Matrix)](https://matrix.to/#/!SuZAJrFcmgupnUNURc:matrix.org?via=matrix.org)
[![Discord](https://img.shields.io/discord/694617177717735457?label=Discord)](https://discord.gg/ZWFNBgR)

# Etopa
### Time-based one-time password authenticator (2FA)
Etopa is a two-factor-authentication app, which runs as a web server and can be accessed using a web browser or using an app.
It is currently under development. If you want to contribute and encounter problems, feel free to ask me for help anytime.
The setup is a bit complicated, so I haven't written an instructions page for contributors yet.
